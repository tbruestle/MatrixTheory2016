clc;
A = [ 6 -2 1; 0 3 0; -12 8 -1 ]
p = poly(A)
r = roots(poly(A))

A3 = A - 3*eye(3,3)
A3p2 = A3*A3
A2 = A - 2*eye(3,3)

V3m = rref([transpose(A3) eye(3,3) ] )
V2m = rref([transpose(A2) eye(3,3) ] )

null2ma = transpose(V2m(3,4:6))
null3a = transpose(V3m(2,4:6))
null3b = transpose(V3m2(3,4:6))

z2a = A*null2a - 2*null2a
z3a = A*null3a - 3*null3a
z3b = (A-3*eye(3,3))^2*null3b
z3c = (A-3*eye(3,3))^2*null3c


J = [ 2, 0 0; 0, 3, 0; 0, 0, 3];

S = [null2a null3a null3b]
J2 = S^-1*A*S
J

