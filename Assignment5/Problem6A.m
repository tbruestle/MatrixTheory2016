clc;

C = [-1 4 6; 7 2 0; -1 6 4; 7 0 2]
Ct = transpose(C)

CtC = Ct*C

pCtC = poly(CtC)
rCtC = roots(pCtC)

CtC8 = CtC - 8*eye(3,3)
CtC96 = CtC - 96*eye(3,3)
CtC108 = CtC - 108*eye(3,3)


V8m = rref([transpose(CtC8) eye(3,3) ] )
V96m = rref([transpose(CtC96) eye(3,3) ] )
V108m = rref([transpose(CtC108) eye(3,3) ] )

v8 = transpose(V8m(3,4:6))
v96 = transpose(V96m(3,4:6))
v108 = transpose(V108m(3,4:6))

CCt = C*Ct
CCt0 = CCt - 0*eye(4,4)
CCt8 = CCt - 8*eye(4,4)
CCt96 = CCt - 96*eye(4,4)
CCt108 = CCt - 108*eye(4,4)


Vt0m = rref([transpose(CCt0) eye(4,4) ] )
Vt8m = rref([transpose(CCt8) eye(4,4) ] )
Vt96m = rref([transpose(CCt96) eye(4,4) ] )
Vt108m = rref([transpose(CCt108) eye(4,4) ] )


vt0 = transpose(Vt0m(4,5:8))
vt8 = transpose(Vt8m(4,5:8))
vt96 = transpose(Vt96m(4,5:8))
vt108 = transpose(Vt108m(4,5:8))

V = [   v8/norm(v8)     v96/norm(v96)     v108/norm(v108)     ]
U = [   vt0/norm(vt0)     -vt8/norm(vt8)     -vt96/norm(vt96)     vt108/norm(vt108)     ]

S = transpose(U)*C*V

U*S*transpose(V)