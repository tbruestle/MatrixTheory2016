A = [	0	5/2	;	-2	-2	]
p = poly(A)
r = roots(p)
lam1=r(1)
I=eye(2,2)
Z = A-lam1*I
R = rref([Z',I])
v = R(2,3:4)'
x = real(v)
y = imag(v)
S = [y x]
Si = inv(S)
Si*A*S
R = rref([Z.',I])
v = R(2,3:4).'