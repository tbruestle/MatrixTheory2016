clc;
A = [ 1 1 0; 0 1 1; 0 0 2 ]
p = poly(A)
r = roots(poly(A))

A1 = A - 1*eye(3,3)
A2 = A - 2*eye(3,3)

V1m = rref([transpose(A1) eye(3,3) ] )
V1ms = rref([transpose(A1*A1) eye(3,3) ] )
V2m = rref([transpose(A2) eye(3,3) ] )

null1a = transpose(V1m(3,4:6))
null1b = transpose(V1ms(3,4:6))
null2a = transpose(V2m(3,4:6))


z1a = A*null1a - 1*null1a
z1b1 = A1*null1b - null1a
z1b2 = A1*A1*null1b
z2a = A*null2a - 2*null2a


J = [ 1, 1, 0; 0, 1, 0; 0, 0, 2];

S = [null1a null1b null2a]
J2 = S^-1*A*S
J


