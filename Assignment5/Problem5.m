clc;

A = [ 2 -8 20 -14 20 -14; 0 -7 23 -16 23 -16; ...
      3 -14 28 -16 24 -17; 5 -14 20 -8 20 -15; ...
      1 -9 17 -10 23 -15; 0 -10 22 -14 26 -16 ]

A2 = A - 2*eye(6,6)
A4 = A - 4*eye(6,6)


V2m = rref([transpose(A2) eye(6,6) ] )
V4m = rref([transpose(A4) eye(6,6) ] )
V4m2 = rref([transpose(A4*A4) eye(6,6) ] )
V4m3 = rref([transpose(A4*A4*A4) eye(6,6) ] )

v4 = transpose(V4m3(5,7:12))
v3 = A4*v4
v2 = A4*v3

v6 = transpose(V4m2(6,7:12))
v5 = A4*v6

v1 = transpose(V2m(6,7:12))

S = [ v1 v2 v3 v4 v5 v6 ]
S^-1*A*S
% null(A2)
% null(A2*A2)
% V2m = rref([transpose(A2) eye(4,4) ] )
% V2m2 = rref([transpose(A2*A2) eye(4,4) ] )
% V3m = rref([transpose(A3) eye(4,4) ] )
% 
% 
% null2a = transpose(V2m(3,5:8))
% null2b = transpose(V2m(4,5:8))
% null2c = transpose(V2ms(2,5:8))
% null3a = transpose(V3m(4,5:8))
% 
% v3 = null2c
% v2 = A2 * null2c
% 
% 
% J = [ 2 1 0 0 ; 0 2 0 0; 0 0 2 0; 0 0 0 3 ]
% 
% S = [ v2 v3 null2b null3a]
% 
% 
% J2 = S^-1*A*S
% J
% % 
% % 
% % 
