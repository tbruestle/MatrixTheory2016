clc;
A = [ 1 1 1; 0 2 1; 0 0 3 ]
p = poly(A)
r = roots(poly(A))

A1 = A - 1*eye(3,3)
A2 = A - 2*eye(3,3)
A3 = A - 3*eye(3,3)

V1m = rref([transpose(A1) eye(3,3) ] )
V2m = rref([transpose(A2) eye(3,3) ] )
V3m = rref([transpose(A3) eye(3,3) ] )

null1a = transpose(V1m(3,4:6))
null2a = transpose(V2m(3,4:6))
null3a = transpose(V3m(3,4:6))


z1a = A*null1a - 1*null1a
z2a = A*null2a - 2*null2a
z3a = A*null3a - 3*null3a


J = [ 1, 0 0; 0, 2, 0; 0, 0, 3];

S = [null1a null2a null3a]
J2 = S^-1*A*S
J


