clc;
A = [
    sym('a11') sym('a12') sym('a13');
    sym('a21') sym('a22') sym('a23');
    sym('a31') sym('a32') sym('a33')
    ];
B1 = A;
B1(1,:) = B1(1,:) + sym('x12')*A(2,:) + sym('x13')*A(3,:);
B2 = A;
B2(2,:) = B2(2,:) + sym('x23')*A(3,:) + sym('x21')*A(1,:);
B3 = A;
B3(3,:) = B3(3,:) + sym('x31')*A(1,:) + sym('x32')*A(2,:);

A
detA = det(A)

B1
detB1 = det(B1)

B2
detB2 = det(B2)

B3
detB3 = det(B3)