clc;
A = [
    sym('a11') sym('a12') sym('a13');
    sym('a21') sym('a22') sym('a23');
    sym('a31') sym('a32') sym('a33')
    ];
B1 = [A(2,:);A(1,:);A(3,:)];
B2 = [A(1,:);A(3,:);A(2,:)];
B3 = [A(3,:);A(2,:);A(1,:)];

A
detA = det(A)

B1
detB1 = det(B1)

B2
detB2 = det(B2)

B3
detB3 = det(B3)