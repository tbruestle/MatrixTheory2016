clc;
A = [ 4-sym('L'), -6, -6; -3, 1-sym('L'), 3;  6, -6, -8-sym('L') ]

eig(A)
detA = det(A)
B = [transpose(A) eye(3)]

L = -2
rrefB = rref(subs(B))
subs(A)
transpose(rrefB(3,4:6))
subs(A)*transpose(rrefB(2:3,4:6))

L = 1
rrefB = rref(subs(B))
subs(A)*transpose(rrefB(3,4:6))
