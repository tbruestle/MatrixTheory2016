clc;
A = [ sym('p11'),sym('p12'),1 - sym('p11') - sym('p12'); ...
    sym('p21'),sym('p22'),1 - sym('p21') - sym('p22'); ...
    1-sym('p11')-sym('p21'),1-sym('p12')-sym('p22'),...
    -1 + sym('p11') + sym('p12') + sym('p21') + sym('p22') ];

p11 = 1/5
p12 = 1/3
p21 = 1/21
p22 = 1/6

A = subs(A)

% 
detA = det(A - eye(3)*sym('L'));
[S,D] = eig(A)
S = simplify(S)
D = simplify(D)
% 
S*[1 0 0; 0 0 0; 0 0 0]*S^-1
% S(1,1)^2 + S(2,1)^2 + S(3,1)^2
% S = [ sym('s11'),sym('s12'),sym('s13'); ...
%     sym('s21'),sym('s22'),sym('s23'); ...
%     sym('s31'),sym('s32'),sym('s33') ];
% A2 = A*S - S*D;
% 
% A3 = sym(zeros(9,9));
% for v = 1:1:9
%     s11 = (v == 1);
%     s12 = (v == 2);
%     s13 = (v == 3);
%     s21 = (v == 4);
%     s22 = (v == 5);
%     s23 = (v == 6);
%     s31 = (v == 7);
%     s32 = (v == 8);
%     s33 = (v == 9);
%     i = subs(A2);
%     A3(1:9,v) = [i(1,1);i(1,2);i(1,3);i(2,1);i(2,2);i(2,3);i(3,1);i(3,2);i(3,3)];
% end
% 
% null(A3)
% 
% A4 = rref([transpose(A3) eye(9)])
% A5 = transpose(A4(9,10:18))
% 
% S2 = [ A4(9,10:12); A4(9,13:15); A4(9,16:18) ]
% A*S2 - S2*D
% 
% [G,S] = spectralfact(A)