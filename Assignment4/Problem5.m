clc;
A = [ 51-sym('L'), -20, 12; -150, 61-sym('L'), -36;  -450, 180, -107-sym('L') ]

eig(A)
detA = det(A)
B = [transpose(A) eye(3)]

L = 1
rrefB = rref(subs(B))
subs(A)
transpose(rrefB(3,4:6))
subs(A)*transpose(rrefB(2:3,4:6))

L = 3
rrefB = rref(subs(B))
subs(A)*transpose(rrefB(3,4:6))


C = [	50      0       0       -20 	0       0       12      0       0	; ...
        0       50      0       0       -20     0       0       12      0	; ...
        0       0       48      0       0       -20     0       0       12	; ...
        -150	0       0       60      0       0       -36     0       0	; ...
        0       -150	0       0       60      0       0       -36     0	; ...
        0       0       -150	0       0       58      0       0       -36	; ...
        -450	0       0       180     0       0       -108	0       0	; ...
        0       -450       0    0       180     0       0       -108	0	; ...
        0       0       -450	0       0       180     0       0       -110	]
    
    
D = [ transpose(C) eye(9) ]
E = rref(D)
E(5:9,10:18)

S = [ 1 0 1; 0 1 -3; -25/6 5/3 -9 ]
D2 = S^-1 * A * S