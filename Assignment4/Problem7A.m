clc;
c = transpose([ sym('c0') sym('c1') sym('c2')  sym('c3') ]);
sizeC = size(c,1);
A = [ zeros(sizeC,sizeC-1) -c];
A(2:sizeC,1:sizeC-1) = eye(sizeC-1);
A = transpose(A)
z = sym(zeros(sizeC,1));
for v = 1:1:sizeC
    z(v,1) = sym('L')^(v-1);
end
(A - eye(sizeC)*sym('L')) * z