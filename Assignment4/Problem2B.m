clc;

x = [ 0 1 0; 1 0 1; 1 0 0 ]
P1 = [ 0 0 0; 0 0 1; 0 0 1 ]
CheckP1 = P1 * x

P2 = [ 1 0 0; 0 1 -1; 0 0 0 ]
CheckP2 = P2 * x

CheckP1xP1 = P1 * P1 - P1
CheckP2xP2 = P2 * P2 - P2
CheckP1pP2 = P1 + P2 - eye(3)
CheckP1xP2 = P1 * P2
CheckP2xP1 = P2 * P1

P1xx = x^-1*P1*x
P2xx = x^-1*P2*x

[eye(4) zeros(4,3)]