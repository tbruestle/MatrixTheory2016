clc;
A = [ sym('p11'),sym('p12'),sym('p13'); ...
    sym('p21'),sym('p22'),sym('p23'); ...
    1-sym('p11')-sym('p21'),1-sym('p12')-sym('p22'),1-sym('p13')-sym('p23') ];
detA = det(A - eye(3)*sym('L'));
[V,D] = eig(A);
V(1:3,1) = V(1:3,1) * (sym('p11') + sym('p22') - sym('p11')*sym('p22') + sym('p12')*sym('p21') - 1)
V(1:3,2) = V(1:3,2) * (2*(sym('p11') - sym('p12') + sym('p21') - sym('p22')))
V(1:3,3) = V(1:3,3) * (2*(sym('p11') - sym('p12') + sym('p21') - sym('p22')))
V = simplify(V)
D = simplify(D)

V(1:3,1);
V(1:3,2);
V(1:3,3);
% simplify()
% simplify(V(1:3,3))


A = [ 1/8,1/3,1/2; ...
    3/8,1/3,1/4; ...
    1/2,1/3,1/4 ];
detA = det(A - eye(3)*sym('L'));
[V,D] = eig(A);

