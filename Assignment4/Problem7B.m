clc;
c = transpose([ sym('c0') sym('c1') sym('c2')  sym('c3') ]);
sizeC = size(c,1);
A = [ zeros(sizeC,sizeC-1) -c];
A(2:sizeC,1:sizeC-1) = eye(sizeC-1);
A = transpose(A)

M = sym(zeros(4,4));
for v = 1:1:4
    M(v,1) = sym('L1')^(v-1);
    M(v,2) = sym('L2')^(v-1);
    M(v,3) = sym('L3')^(v-1);
    M(v,4) = sym('L4')^(v-1);
end

detM = simplify(det(M))
% (A - eye(sizeC)*sym('L')) * z
