A = [ -103-sym('L'), 38, -24; -200, 74-sym('L'), -46;  120, -44, 29-sym('L') ]


detA = det(A)
B = [transpose(A) eye(3)]

L = -3
rrefB = rref(subs(B))
subs(A)
transpose(rrefB(3,4:6))
subs(A)*transpose(rrefB(3,4:6))

L = 2
rrefB = rref(subs(B))
subs(A)*transpose(rrefB(3,4:6))

L = 1
rrefB = rref(subs(B))
subs(A)*transpose(rrefB(3,4:6))
