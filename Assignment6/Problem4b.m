x1 = [ 0; -5; -1 ]
x2 = [ 3; 5; 2 ]

x = [ 3; 10; 3 ]

inner_x1_x1 = transpose(x1)*x1
inner_x1_x2 = transpose(x2)*x1
inner_x2_x1 = transpose(x1)*x2
inner_x2_x2 = transpose(x2)*x2

inner_x_x1 = transpose(x1)*x
inner_x_x2 = transpose(x2)*x

M = [ inner_x1_x1 inner_x2_x1 inner_x_x1; ...
    inner_x1_x2 inner_x2_x2 inner_x_x2 ]

rrefM = rref(M)