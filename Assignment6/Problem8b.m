clc;
A = [ -2 0 2 -2; -3 3 9 -3; -1 0 1 -1; -2 0 2 -2 ]

AI = [ A  eye(4) ]

AhatB = rref(AI)

AhatB*15
Ahat = AhatB(:,1:4)
B = AhatB(:,5:8)
Bi = B^-1

A1 = Bi(:,1:2)
A2 = Ahat(1:2,:)

M1 = (transpose(A1)*A1)^-1*transpose(A1)

M2 = transpose(A2)*(A2*transpose(A2))^-1


Ap = M2*M1
Ap*99

test1 = A*Ap*A-A
test2 = Ap*A*Ap-Ap
