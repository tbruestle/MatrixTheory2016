clc;
x1 = [1; 0; 1]
x2 = [1; 0; 0]
x3 = [0; 1; 0]

Q = [1, 2, 1; 2, 5, 5; 1, 5, 26]

y1 = x1

inner_x2_y1 = transpose(y1)*Q*x2
inner_y1_y1 = transpose(y1)*Q*y1
y2 = x2 - y1*inner_x2_y1/inner_y1_y1

inner_x3_y1 = transpose(y1)*Q*x3
inner_y1_y1 = transpose(y1)*Q*y1

inner_x3_y2 = transpose(y2)*Q*x3
inner_y2_y2 = transpose(y2)*Q*y2

y3 = x3 - y1*inner_x3_y1/inner_y1_y1 - y2*inner_x3_y2/inner_y2_y2
