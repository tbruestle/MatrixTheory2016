clc;
x1 = [1; -1; -1; 1]
x2 = [-3; 3; -1; 1]
x3 = [-5; 5; -3; 3]

y1 = x1
y2 = x2 - y1*(transpose(x2)*y1)/(transpose(y1)*y1)
y3 = x3 - y1*(transpose(x3)*y1)/(transpose(y1)*y1) - y2*(transpose(x3)*y2)/(transpose(y2)*y2)