clc;
A = [ 1, 2; 3, 1; 3, 4 ]
y = [ 3; 4; 8 ]

% [U,S,V] = svd(A)
% A2 = U*S*V'
% Sp = [ 1/S(1,1) 0 0; 0 1/S(2,2) 0 ]
% Sp2 = (S'*S)^-1*S'
% Ap2 = V*Sp*U'

Ap = (A'*A)^-1*A'


E = Ap*y
ya = A*E

Ap*(y-ya)